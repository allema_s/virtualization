# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require udev-rules

SUMMARY="Spice agent for Linux"
DESCRIPTION="
* Client mouse mode (no need to grab mouse by client, no mouse lag)
  this is handled by the daemon by feeding mouse events into the kernel
  via uinput. This will only work if the active X-session is running a
  spice-vdagent process so that its resolution can be determined.
* Automatic adjustment of the X-session resolution to the client resolution
  for single monitor configurations.
* Support of copy and paste (text and images) between the active X-session
  and the client. This supports both the primary selection and the clipboard.
* Limited support for multiple displays
"
HOMEPAGE="http://www.spice-space.org"
DOWNLOADS="${HOMEPAGE}/download/releases/${PNV}.tar.bz2"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/pkg-config
        virtualization-lib/spice-protocol[>=0.12.8]
        x11-proto/xorgproto
    build+run:
        dev-libs/glib:2[>=2.28]
        sys-apps/dbus
        sys-apps/systemd[>=209]
        sys-sound/alsa-lib[>=1.0.22]
        x11-libs/libpciaccess
        x11-libs/libX11
        x11-libs/libXfixes
        x11-libs/libXinerama
        x11-libs/libXrandr[>=1.3]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-pciaccess
    --disable-static-uinput
    --with-init-script=systemd
    --with-session-info=systemd
)

src_install() {
    default

    for dir in tmpfiles.d modules-load.d ; do
    # Move file(s) into the correct dir
        dodir /usr/$(exhost --target)/lib/${dir}
        if [[ -d ${IMAGE}/usr/lib/${dir} ]]; then
            edo mv "${IMAGE}"/usr/lib/${dir}/* "${IMAGE}"/usr/$(exhost --target)/lib/${dir}/
            edo rmdir "${IMAGE}"/usr/lib/{${dir},}
        fi
        if [[ -d ${IMAGE}/etc/${dir} ]]; then
            edo mv "${IMAGE}"/etc/${dir}/* "${IMAGE}"/usr/$(exhost --target)/lib/${dir}/
            edo rmdir "${IMAGE}"/etc/${dir}
        fi
    done

    dodir "${UDEVRULESDIR}"
    edo mv "${IMAGE}"/lib/udev/rules.d/70-spice-vdagentd.rules "${IMAGE}"/"${UDEVRULESDIR}"
    edo rmdir "${IMAGE}"/lib/{udev/{rules.d,},}

    # Remove empty dirs...
    edo rmdir "${IMAGE}"/var/run/{spice-vdagentd,}
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/modules-load.d

    # ... and keep one
    keepdir /var/log/spice-vdagentd
}

