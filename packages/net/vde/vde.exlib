# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}2-${PV}

SUMMARY="${PN} is a virtual distributed ethernet emulator"
DESCRIPTION="
VDE: Virtual Distributed Ethernet. It creates the abstraction of a virtual
ethernet: a single vde can be accessed by virtual and real computers.
VDE is primarily useful for virtualisation software like kvm, qemu, bochs, uml etc.
"
HOMEPAGE="http://${PN}.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/${PN}/${MY_PNV}.tar.bz2"

REMOTE_IDS="sourceforge:${PN}"

UPSTREAM_DOCUMENTATION="http://wiki.virtualsquare.org/ [[ lang = en ]]"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="
    pcap [[ description = [ Enable the VDE packet dump plugin using libpcap for package capturing with pdump ] ]]
    ssl  [[ description = [ Enable the VDE encrypted virtual cable (VDE CryptCab) ] ]]

    ssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build+run:
        pcap? ( dev-libs/libpcap )
        ssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
    run:
        group/vde
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-python
    --disable-profile
    --enable-experimental
    --enable-tuntap
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    pcap
    "ssl cryptcab"
)

# Upstream broke parallel compilation in 2.3.2.
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

WORK=${WORKBASE}/${MY_PNV}

